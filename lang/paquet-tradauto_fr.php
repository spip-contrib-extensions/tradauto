<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-tradauto
// Langue: fr
// Date: 29-10-2012 14:12:31
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'tradauto_description' => 'Traduction automatique des données éditoriales (articles, rubriques, etc.) dans l\'espace privé.',
	'tradauto_slogan' => 'Partez à la conquête du Monde avec le Traducteur automatique',
);
?>